class Task1Result:
    def __init__(self, name, white=0, black=0, pink=0, blue=0, green=0, brown=0, yellow=0, red=0):
        super().__init__()
        delimiter = name.split('/')
        last_part = delimiter[-1]
        self.name = last_part.replace('_gt', '').replace('.txt', '').replace('.jpg', '')
        self.white = min(white, 1)
        self.black = min(black, 1)
        self.pink = min(pink, 1)
        self.blue = min(blue, 1)
        self.green = min(green, 1)
        self.brown = min(brown, 1)
        self.yellow = min(yellow, 1)
        self.red = min(red, 15)

    def get_total(self):
        print(self.white + self.black + self.pink + \
               self.blue + self.green + self.brown + self.yellow + self.red)
        return self.white + self.black + self.pink + \
               self.blue + self.green + self.brown + self.yellow + self.red
