import glob
import os

from task1_result import Task1Result


class GroundTruthReader:
    @staticmethod
    def scenario_1():
        base_folder = os.path.join('training_data', 'Task1', 'ground-truth')
        file_names = glob.glob(os.path.join(base_folder, '*_gt.txt'))

        results = []
        for file_name in file_names:
            with open(file_name) as f:
                content = f.readlines()
                result = Task1Result(name=file_name)
                for i in range(1, len(content)):
                    number_of_balls = content[i].split(' ')[0]
                    if i == 1:
                        result.white = number_of_balls
                    if i == 2:
                        result.black = number_of_balls
                    if i == 3:
                        result.pink = number_of_balls
                    if i == 4:
                        result.blue = number_of_balls
                    if i == 5:
                        result.green = number_of_balls
                    if i == 6:
                        result.brown = number_of_balls
                    if i == 7:
                        result.yellow = number_of_balls
                    if i == 8:
                        result.red = number_of_balls

                results.append(result)

        return results
