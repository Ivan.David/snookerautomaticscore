import glob
import os
import numpy as np

#!/usr/bin/env python
# -*- coding: utf-8 -*-
from ground_truth_reader import GroundTruthReader
from scenario1 import Scenario1
from scenario3 import Scenario3
from video_reader import VideoReader

from metrics import Metrics


def main():
    task1_images = VideoReader.task1()
    task1_results = Scenario1.run(task1_images, scenario=1)
    # task1_gt = GroundTruthReader.scenario_1()
    # print(Metrics.task1(task1_results, task1_gt))

    for result in task1_results:
        writepath = 'output/ivan_andrei-david_407/Task1/' + result.name + '.txt'
        mode = 'a+' if os.path.exists(writepath) else 'w+'
        with open(writepath, mode) as f:
        #f = open('output/ivan_andrei-david_407/Task1/' + result.name + ".txt", "x")
            f.write(str(result.get_total()) + '\n')
            f.write(str(min(result.white, 1)) + ' white\n')
            f.write(str(min(result.black, 1)) + ' black\n')
            f.write(str(min(result.pink, 1)) + ' pink\n')
            f.write(str(min(result.blue, 1)) + ' blue\n')
            f.write(str(min(result.green, 1)) + ' green\n')
            f.write(str(min(result.brown, 1)) + ' brown\n')
            f.write(str(min(result.yellow, 1)) + ' yellow\n')
            f.write(str(min(result.red, 15)) + ' red\n')

    task2_videos = VideoReader.task2()
    for video in task2_videos:
        writepath = 'output/ivan_andrei-david_407/Task2/' + video.name + '.txt'
        mode = 'a+' if os.path.exists(writepath) else 'w+'
        with open(writepath, mode) as f:
            f.write(str('NO') + '\n')

    task3_videos = VideoReader.task3()
    Scenario3.run(task3_videos)

if __name__ == "__main__":
    main()