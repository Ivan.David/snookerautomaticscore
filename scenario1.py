import cv2 as cv
import numpy as np
from skimage.feature import blob_dog, blob_log, blob_doh
import matplotlib.pyplot as plt
from math import sqrt
import os
import glob

from image import Image
from task1_result import Task1Result


class TableKeypoint:
    def __init__(self, name, x, y) -> None:
        super().__init__()
        self.name = name
        self.x = x
        self.y = y


class Scenario1:
    @staticmethod
    def run(images, scenario):
        results_list = []
        for image in images:
            # table = Scenario1.find_table(image.image)
            table = Scenario1.find_table_template_matching(image.image)
            result_dict = Scenario1.detect_balls_using_hsv(table, True)
            results_list.append(Task1Result(name=image.name, white=result_dict['white'], black=result_dict['black'],
                                            red=result_dict['red'], yellow=result_dict['yellow'],
                                            brown=result_dict['brown'], pink=result_dict['pink'],
                                            green=result_dict['green'], blue=result_dict['blue']))

        return results_list

    @staticmethod
    def find_table_template_matching(frame):
        base_folder_matching = 'templates_2'
        images_names = glob.glob(os.path.join(base_folder_matching, "*.png"))
        templates = []
        for image_name in images_names:
            template = Image(image_name)
            templates.append(template)
            # cv.imshow("template", template)
            # cv.waitKey(0)
            # cv.destroyAllWindows()

        frame_copy = frame.copy()

        # All the 6 methods for comparison in a list
        methods = ['cv.TM_CCOEFF_NORMED']

        # methods = ['cv.TM_CCOEFF_NORMED']

        # iau median value de la top left si bottom right, si daca nu e in median value nu il mai adaug
        # calculez colturile mesei pe baza a cel mai de sus top left si cel mai din stanga cel ami de sus top left
        # same for bottom

        idx = -1
        corners = []
        pixel_sum = 0
        for template in templates:
            idx = idx + 1
            template_gray = cv.cvtColor(template.image, cv.COLOR_BGR2GRAY)
            w, h = template_gray.shape[::-1]
            frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
            h1, w1 = frame_gray.shape
            frame_gray = frame_gray[:, int(0.15 * w1): int(0.85 * w1)]

            method = eval('cv.TM_CCOEFF_NORMED')
            frame = frame_copy.copy()

            # apply template Matching
            res = cv.matchTemplate(frame_gray, template_gray, method)

            min_val, max_val, min_loc, max_loc = cv.minMaxLoc(res)

            # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
            if method in [cv.TM_SQDIFF, cv.TM_SQDIFF_NORMED]:
                top_left = min_loc
            else:
                top_left = max_loc

            bottom_right = (top_left[0] + w, top_left[1] + h)

            cv.rectangle(frame_gray, top_left, bottom_right, 255, 2)
            corners.append(TableKeypoint(name=template.name, x=top_left[0], y=top_left[1]))

            # cv.imshow("Template_matching " + template.name, frame_gray)
            # cv.waitKey(0)
            # cv.destroyAllWindows()

        # ...gasit masa cu totul
        bottom_left = (0, 0)
        bottom_right = (0, 0)
        middle_left = (0, 0)
        middle_right = (0, 0)
        top_left = (0, 0)
        top_right = (0, 0)

        # insert top left, top_right si bottom_left, bottom_right, calculate din astea

        for corner in corners:
            if corner.name == 'templates_2/midLeft.png':
                middle_left = (corner.x, corner.y)

            if corner.name == 'templates_2/midRight.png':
                middle_right = (corner.x, corner.y)

            if corner.name == 'templates_2/bottomLeft.png':
                bottom_left = (corner.x, corner.y)

            if corner.name == 'templates_2/bottomRight.png':
                bottom_right = (corner.x, corner.y)

            if corner.name == 'templates_2/topRight.png':
                top_right = (corner.x, corner.y)

            if corner.name == 'templates_2/topLeft.png':  # leftUp
                top_left = (corner.x, corner.y)

            deduct_bot_left = False
            deduct_bot_right = False
            deduct_top_left = False
            deduct_top_right = False
            poly_array = []

        masked_table = np.zeros((frame.shape[0], frame.shape[1]), np.uint8)

        a3 = np.array([[[bottom_left[0] + int(0.19 * w1), bottom_left[1] + int(0.01 * h1)],
                        [middle_left[0] + int(0.18 * w1), middle_left[1]], [top_left[0] + int(0.18 * w1), top_left[1] + int(0.02 * h1)],
                        [top_right[0] + int(0.13 * w1), top_right[1] + int(0.02 * h1)],
                        [middle_right[0] + int(0.13 * w1), middle_right[1]],
                        [bottom_right[0] + int(0.13 * w1), bottom_right[1] + int(0.01 * h1)]]], dtype=np.int32)
        cv.fillPoly(masked_table, a3, 255)

        # cv.rectangle(masked_table, (x2, y2), (x1, y1), 255, -1)
        # masked_table[highest_top:lowest_bottom, leftest_point:rightest_point] = 255

        # masked_table =np.zeros(frame.shape, np.uint8)
        #
        # frame = frame[:, int(0.15 * w1): int(0.85 * w1)]
        # smaller_frame_mask = np.zeros(frame.shape, np.uint8)
        # frame = frame[lowest_bottom:highest_top, leftest_point:rightes_point]
        # smaller_frame_mask[lowest_bottom:highest_top, leftest_point:rightes_point, :] = 255
        # masked_table += sm

        img = cv.bitwise_and(frame, frame, mask=masked_table)
        # cv.imshow('img_contour', img)
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        return img

    @staticmethod
    def find_table(frame):
        # back_sub = cv.createBackgroundSubtractorMOG2()
        # fgMask = back_sub.apply(frame)
        # cv.rectangle(frame, (10, 2), (100, 20), (255, 255, 255), -1)
        #
        # cv.imshow('Frame', frame)
        # cv.waitKey(0)
        # cv.imshow('FG Mask', fgMask)
        # cv.waitKey(0)
        low_green = (46, 100, 0)
        high_green = (85, 255, 255)

        frame_hsv = cv.cvtColor(frame, cv.COLOR_BGR2HSV)
        mask_table_hsv = cv.inRange(frame_hsv, low_green, high_green)
        kernel = np.ones((15, 15), np.uint8)

        mask_table_hsv = cv.dilate(mask_table_hsv, kernel, iterations=2)
        mask_table_hsv = cv.erode(mask_table_hsv, kernel, iterations=2)

        table = cv.bitwise_and(frame, frame, mask=mask_table_hsv)
        # cv.imshow('mask_table_hsv', table)
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        _, contours, _ = cv.findContours(mask_table_hsv, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
        max_area = 0
        img_contour = np.zeros(table.shape, np.uint8)

        # take the largest in area contour
        if len(contours) > 0:
            cnt = contours[0]
            for i, cont in enumerate(contours):
                contour_area = cv.contourArea(cont)
                if contour_area > max_area:
                    max_area = contour_area
                    cnt = contours[i]

            epsilon = 0.01 * cv.arcLength(cnt, True)  # contour perimeter
            approx = cv.approxPolyDP(cnt, epsilon, True)  # get the approximation
            cv.drawContours(img_contour, [approx], -1, (255, 255, 255), -1)
            ray_image = cv.cvtColor(img_contour, cv.COLOR_BGR2GRAY)
            img = cv.bitwise_and(table, table, mask=ray_image)
            # cv.imshow('img_contour', img)
            # cv.waitKey(0)
            # cv.destroyAllWindows()

            return img
        else:
            return table

    @staticmethod
    def detect_balls_using_hsv(table, show_results=True):
        _, _, v1 = cv.split(table)
        balls_number = 0
        balls_color_dict = {"black": 0,
                            "blue": 0,
                            "brown": 0,
                            "green": 0,
                            "pink": 0,
                            "red": 0,
                            "white": 0,
                            "yellow": 0}

        table_hsv = cv.cvtColor(table, cv.COLOR_BGR2HSV)

        # snooker balls based on color
        low_red = np.array([0, 0, 90])
        high_red = np.array([255, 200, 255])
        mask_red = cv.inRange(table_hsv, low_red, high_red)
        red_ball = cv.bitwise_and(table, table, mask=mask_red)

        low_white = np.array([90, 200, 65])
        high_white = np.array([255, 255, 255])
        mask_white = cv.inRange(table_hsv, low_white, high_white)
        white_ball = cv.bitwise_and(table, table, mask=mask_white)

        low_black = np.array([80, 90, 50])
        high_black = np.array([255, 170, 150])
        mask_black = cv.inRange(table_hsv, low_black, high_black)
        black_ball = cv.bitwise_and(table, table, mask=mask_black)

        low_pink = np.array([90, 0, 160])
        high_pink = np.array([255, 245, 255])
        mask_pink = cv.inRange(table_hsv, low_pink, high_pink)
        pink_ball = cv.bitwise_and(table, table, mask=mask_pink)

        low_blue = np.array([91, 0, 0])
        high_blue = np.array([180, 135, 55])
        mask_blue = cv.inRange(table, low_blue, high_blue)
        # low_blue = np.array([100, 0, 160])
        # high_blue = np.array([150, 255, 150])
        # mask_blue = cv.inRange(table_hsv, low_blue, high_blue)
        blue_ball = cv.bitwise_and(table, table, mask=mask_blue)

        low_brown = np.array([0, 35, 80])
        high_brown = np.array([180, 255, 255])
        mask_brown = cv.inRange(table_hsv, low_brown, high_brown)
        brown_ball = cv.bitwise_and(table, table, mask=mask_brown)

        low_green = np.array([60, 100, 0])
        high_green = np.array([255, 255, 200])
        mask_green = cv.inRange(table_hsv, low_green, high_green)
        green_ball = cv.bitwise_and(table, table, mask=mask_green)

        low_yellow = np.array([0, 100, 100])
        high_yellow = np.array([130, 255, 255])
        mask_yellow = cv.inRange(table_hsv, low_yellow, high_yellow)
        yellow_ball = cv.bitwise_and(table, table, mask=mask_yellow)

        mask_balls = mask_white + mask_red + mask_blue + mask_pink + mask_green + mask_brown + mask_yellow + mask_black

        balls = cv.bitwise_and(table, table, mask=mask_balls)
        # cv.imshow('balls mask', balls)
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        _, _, ceva = cv.split(balls)
        altceva = cv.GaussianBlur(ceva, (5, 5), 0)
        circles = cv.HoughCircles(altceva, cv.HOUGH_GRADIENT, 1, 10,
                                  param1=50, param2=7, minRadius=3, maxRadius=7)

        circles = np.uint16(np.around(circles))
        for i in circles[0, :]:
            # draw the outer circle
            cv.circle(ceva, (i[0], i[1]), i[2], (255, 255, 0), 2)
            # draw the center of the circle
            cv.circle(ceva, (i[0], i[1]), 2, (0, 0, 255), 3)

        # cv.imshow('detected circles', ceva)
        # cv.waitKey(0)
        # cv.destroyAllWindows()
        # if show_results:
        #     # cv.imshow("Table", table)
        #     cv.imshow("BALLS", balls)
        #     # cv.imshow("Mask balls", mask_balls)
        #     #
        #     cv.waitKey(0)
        #     cv.destroyAllWindows()
        #
        # _, _, grasyscale = cv.split(balls)
        # v1 = cv.blur(v1,(6,6))
        # v1 = cv.bilateralFilter(v1, 9, 80, 80)
        balls = cv.GaussianBlur(v1, (3, 3), 0)
        balls = cv.erode(balls, None, iterations=1)
        balls = cv.dilate(balls, None, iterations=1)

        alpha = 2.5  # Contrast control (1.0-3.0)
        beta = 10  # Brightness control (0-100)

        v1 = cv.convertScaleAbs(balls, alpha=alpha, beta=beta)

        blobs_log = blob_log(v1, min_sigma=0.8, max_sigma=8, num_sigma=50, threshold=.17, overlap=0.3, )

        # Compute radii in the 3rd column.
        blobs_log[:, 2] = blobs_log[:, 2] * sqrt(2)
        #
        # blobs_dog = blob_dog(grasyscale, max_sigma=7, threshold=.01)
        # blobs_dog[:, 2] = blobs_dog[:, 2] * sqrt(2)
        #
        # blobs_doh = blob_doh(grasyscale, min_sigma=3, max_sigma=20, threshold=.01)

        # ball_radius_upper_threshold = 10
        # ball_radius_lower_threshold = 2
        # filtered_blobs = []
        # for blob in blobs_log:
        #     if ball_radius_lower_threshold < blob[2] < ball_radius_upper_threshold:
        #         filtered_blobs.append(blob)
        #
        fig, ax = plt.subplots(1)
        ax.imshow(table[:, :, [2,1,0]])
        for blob in blobs_log:
            balls_number += 1
            y, x, r = blob
            if r < 8:
                r = 8

            x = int(x)
            y = int(y)

            ball_patch = table[y - 8:y + 8, x - 8:x + 8, :]
            balls_color_dict[Scenario1.detect_patch_color(ball_patch)] += 1

            # cv.imshow('detected circles', ball_patch)
            # cv.waitKey(0)
            # cv.destroyAllWindows()

            circ = plt.Circle((x, y), r, color=Scenario1.detect_patch_color(ball_patch), linewidth=1, fill=False)
            ax.add_patch(circ)

        plt.show()
        print(balls_color_dict)
        return balls_color_dict

    @staticmethod
    def detect_patch_color(patch):
        color_dict = {0: "black",
                      1: "blue",
                      2: "brown",
                      3: "green",
                      4: "pink",
                      5: "red",
                      6: "white",
                      7: "yellow"}

        templates = []
        base_folder_matching = 'template_matching'
        images_names = glob.glob(os.path.join(base_folder_matching, "*.jpg"))
        images_names = sorted(images_names)
        for image_name in images_names:
            template = cv.imread(image_name)
            templates.append(template)
            # cv.imshow("template", template)
            # cv.waitKey(2000)
            # cv.destroyAllWindows()

        mask = np.zeros(patch.shape[:2], dtype="uint8")
        cv.circle(mask, (8, 10), 6, (255, 255, 255), thickness=-1)
        # cv.imshow("mask ", mask)
        # cv.waitKey(0)
        # cv.destroyAllWindows()

        hist_img = cv.calcHist([patch], [0, 1, 2], None, [4, 4, 4], [0, 256, 0, 256, 0, 256])

        hist_templates = []
        for template in templates:
            template_hist = cv.calcHist([template], [0, 1, 2], None, [4, 4, 4], [0, 256, 0, 256, 0, 256])
            hist_templates.append(template_hist)

        distances = []
        for i in range(len(templates)):
            hist_img_norm = hist_img / (hist_img.sum())
            hist_template_norm = hist_templates[i] / (hist_templates[i].sum())
            dist = cv.compareHist(hist_img_norm, hist_template_norm, cv.HISTCMP_CHISQR_ALT)
            distances.append(dist)
        print(distances)
        print(color_dict[np.argmin(distances)])
        return color_dict[np.argmin(distances)]
