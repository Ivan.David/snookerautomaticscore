import cv2 as cv
import numpy as np
import os

class Scenario3:
    @staticmethod
    def run(videos):
        frames_1_total = []
        frames_2_total = []
        for video in videos:
            tracker = cv.TrackerMIL_create()
            initBB = None

            frames_1 = [(video.number_of_frames, -1, -1, -1, -1)]
            frames_2 = [(video.number_of_frames, -1, -1, -1, -1)]
            for i in range(0, int(video.number_of_frames)):
                _, new_frame = video.video.read()

                if new_frame is None:
                        break

                if initBB is not None:
                    (sucessfully, box) = tracker.update(new_frame)

                    if sucessfully:
                        top_left = (int(box[0]), int(box[1]))
                        bottom_right = (int(box[0] + box[2]), int(box[1] + box[3]))
                        cv.rectangle(new_frame, top_left, bottom_right, (255, 0, 0), 2, 1)
                        annotation = (i, top_left[0], top_left[1], bottom_right[0], bottom_right[1])
                        frames_1.append(annotation)
                            #f.write('{} {} {} {} {} \n'.format(i, top_left[0], top_left[1], bottom_right[0], bottom_right[1]))
                        print('frame {}'.format(i))
                    else:
                        print('u')

                    # cv.imshow('current_frame', new_frame)
                    # cv.waitKey(0)
                    # cv.destroyAllWindows()
                if initBB is None:
                    initBB = (int(video.x_min), int(video.y_min), int(video.x_max) - int(video.x_min), int(video.y_max) - int(video.y_min))
                    tracker.init(new_frame, initBB)

            for i in range(0, int(video.number_of_frames)):
                _, new_frame = video.video.read()

                if new_frame is None:
                        break

                if initBB is not None:
                    (sucessfully, box) = tracker.update(new_frame)

                    if sucessfully:
                        top_left = (int(box[0]), int(box[1]))
                        bottom_right = (int(box[0] + box[2]), int(box[1] + box[3]))
                        cv.rectangle(new_frame, top_left, bottom_right, (255, 0, 0), 2, 1)
                        annotation = (i, top_left[0], top_left[1], bottom_right[0], bottom_right[1])
                        frames_2.append(annotation)
                            #f.write('{} {} {} {} {} \n'.format(i, top_left[0], top_left[1], bottom_right[0], bottom_right[1]))
                        print('frame {}'.format(i))
                    else:
                        print('u')

                    # cv.imshow('current_frame', new_frame)
                    # cv.waitKey(0)
                    # cv.destroyAllWindows()
                if initBB is None:
                    initBB = (int(video.x_min2), int(video.y_min2), int(video.x_max2) - int(video.x_min2), int(video.y_max) - int(video.y_min))
                    tracker.init(new_frame, initBB)

            print(video.name)
            writepath = 'output/ivan_andrei-david_407/Task3/' + video.name + '_ball_1.txt'
            mode = 'a+' if os.path.exists(writepath) else 'w+'
            with open(writepath, mode) as f:
                for line in frames_1:
                    f.write('{} {} {} {} {} \n'.format(line[0], line[1], line[2], line[3], line[4]))

            writepath = 'output/ivan_andrei-david_407/Task3/' + video.name + '_ball_2.txt'
            mode = 'a+' if os.path.exists(writepath) else 'w+'
            with open(writepath, mode) as f:
                for line in frames_1:
                    f.write('{} {} {} {} {} \n'.format(line[0], line[1], line[2], line[3], line[4]))

