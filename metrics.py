class Metrics:
    @staticmethod
    def task1(results, groundtruths):
        def myFunc(result):
            return result.name

        results.sort(reverse=True, key=myFunc)
        groundtruths.sort(reverse=True, key=myFunc)

        correct_results = 0
        # sort by name
        for j in range(0, len(groundtruths)):
            groundtruth = groundtruths[j]
            for i in range(0, len(results)):
                result = results[i]

                if groundtruth.name == result.name:
                    if int(result.white) == int(groundtruth.white) and int(result.black) == int(
                            groundtruth.black) and int(result.pink) == int(groundtruth.pink) and int(
                            result.blue) == int(groundtruth.blue) and int(result.green) == int(
                            groundtruth.green) and int(result.brown) == int(groundtruth.brown) and int(
                            result.yellow) == int(groundtruth.yellow) and int(result.red) == int(groundtruth.red):
                        correct_results += 1

        return correct_results / len(results)
