import glob
import os
import cv2 as cv

from image import Image, Video, Video3


class VideoReader:
    @staticmethod
    def task1():
        base_folder = os.path.join('input', 'Task1')
        images_names = glob.glob(os.path.join(base_folder, '*.jpg'))

        task1_images = []
        for i in range(0, len(images_names)):
            print('Reading image %d...' % i)
            image = Image(images_names[i])
            task1_images.append(image)

        return task1_images

    @staticmethod
    def task2():
        base_folder = os.path.join('input', 'Task2')
        videos_names = glob.glob(os.path.join(base_folder, '*.mp4'))

        task2_videos = []
        for i in range(0, len(videos_names)):
            video = Video(videos_names[i])
            task2_videos.append(video)

        return task2_videos

    @staticmethod
    def task3():
        base_folder = os.path.join('input', 'Task3')
        videos_names = glob.glob(os.path.join(base_folder, '*.mp4'))

        task3_videos = []
        for i in range(0, len(videos_names)):
            name = videos_names[i].split('/')[-1].replace('.mp4', '')
            ball1_path = base_folder + '/{}_ball_1.txt'.format(name)
            ball2_path = base_folder + '/{}_ball_2.txt'.format(name)

            no_of_frames = ''
            x_min = ''
            x_max = ''
            y_min = ''
            y_max = ''
            x_min2 = ''
            x_max2 = ''
            y_min2 = ''
            y_max2 = ''
            with open(ball1_path) as fp:
                for cnt, line in enumerate(fp):
                    if cnt == 0:
                        no_of_frames = line.split(' ')[0]
                    else:
                        x_min = line.split(' ')[1]
                        y_min = line.split(' ')[2]
                        x_max = line.split(' ')[3]
                        y_max = line.split(' ')[4].replace('\n', '')

            with open(ball2_path) as fp:
                for cnt, line in enumerate(fp):
                    if cnt == 1:
                        x_min2 = line.split(' ')[1]
                        y_min2 = line.split(' ')[2]
                        x_max2 = line.split(' ')[3]
                        y_max2 = line.split(' ')[4].replace('\n', '')

            video = Video3(videos_names[i], number_of_frames=no_of_frames, x_max=x_max, x_min=x_min, y_max=y_max,
                           y_min=y_min, x_max2=x_max2, x_min2=x_min2, y_min2=y_min2, y_max2=y_max2)
            task3_videos.append(video)

        return task3_videos

    @staticmethod
    def fromTrainingImages():
        base_folder = os.path.join('training_data', 'Task1')
        images_names = glob.glob(os.path.join(base_folder, '*.jpg'))

        task1_images = []
        for i in range(0, 20):  # len(images_names)):
            print('Reading image %d...' % i)
            image = Image(images_names[i])
            task1_images.append(image)

        return task1_images

    @staticmethod
    def scenario2Training():
        base_folder = os.path.join('training_data', 'Task2')
        videos_names = glob.glob(os.path.join(base_folder, '*.mp4'))

        task2_videos = []
        for i in range(0, len(videos_names)):
            video = Video(videos_names[i])
            task2_videos.append(video)

        return task2_videos

    @staticmethod
    def scenario3Training():
        base_folder = os.path.join('training_data', 'Task3')
        videos_names = glob.glob(os.path.join(base_folder, '*.mp4'))
        ball1_tracking = glob.glob(os.path.join(base_folder, '*_ball_1.txt'))
        ball2_tracking = glob.glob(os.path.join(base_folder, '*_ball_2.txt'))

        task3_videos = []
        for i in range(0, 2):#len(videos_names)):
            name = videos_names[i].split('/')[-1].replace('.mp4', '')
            ball1_path = base_folder + '/{}_ball_1.txt'.format(name)
            ball2_path = base_folder + '/{}_ball_2.txt'.format(name)

            no_of_frames = ''
            x_min = ''
            x_max = ''
            y_min = ''
            y_max = ''
            x_min2 = ''
            x_max2 = ''
            y_min2 = ''
            y_max2 = ''
            with open(ball1_path) as fp:
                for cnt, line in enumerate(fp):
                    if cnt == 0:
                        no_of_frames = line.split(' ')[0]
                    else:
                        x_min = line.split(' ')[1]
                        y_min = line.split(' ')[2]
                        x_max = line.split(' ')[3]
                        y_max = line.split(' ')[4].replace('\n', '')

            with open(ball2_path) as fp:
                for cnt, line in enumerate(fp):
                    if cnt == 1:
                        x_min2 = line.split(' ')[1]
                        y_min2 = line.split(' ')[2]
                        x_max2 = line.split(' ')[3]
                        y_max2 = line.split(' ')[4].replace('\n', '')

            video = Video3(videos_names[i], number_of_frames=no_of_frames, x_max=x_max, x_min=x_min, y_max=y_max,
                           y_min=y_min, x_max2=x_max2, x_min2=x_min2, y_min2=y_min2, y_max2=y_max2)
            task3_videos.append(video)

        return task3_videos
