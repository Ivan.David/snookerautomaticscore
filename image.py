import cv2 as cv


class Image:
    def __init__(self, name):
        self.name = name
        self.image = cv.imread(name)


class Video:
    def __init__(self, name):
        split_name = name.split('/')[-1]
        self.name = split_name.replace('.mp4', '')
        self.video = cv.VideoCapture(name)


class Video3:
    def __init__(self, name, number_of_frames, x_min, y_min, x_max, y_max, x_min2, y_min2, x_max2, y_max2):
        split_name = name.split('/')[-1]
        self.name = split_name.replace('.mp4', '')
        self.video = cv.VideoCapture(name)
        self.x_min = x_min
        self.x_max = x_max
        self.y_min = y_min
        self.y_max = y_max
        self.x_min2 = x_min2
        self.x_max2 = x_max2
        self.y_min2 = y_min2
        self.y_max2 = y_max2
        self.number_of_frames = number_of_frames
